// bullyNonStarter.cpp

#include "bully_non_starter.hpp"

#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"

#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"

#include "ns3/random-variable-stream.h"

NS_LOG_COMPONENT_DEFINE("BullyNonStarter");


namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(BullyNonStarter);

// register NS-3 type
TypeId
BullyNonStarter::GetTypeId()
{
  static TypeId tid = TypeId("BullyNonStarter").SetParent<ndn::App>().AddConstructor<BullyNonStarter>();
  return tid;
}

// Processing upon start of the application
void
BullyNonStarter::StartApplication()
{
  // initialize ndn::App
  ndn::App::StartApplication();
  std::cout << "Starting Application bullyNonStarter.." << Simulator::Now() << std::endl;

  // Add entry to FIB for `/prefix/sub`
  ndn::FibHelper::AddRoute(GetNode(), "/readIDs" , m_face, 0);
  ndn::FibHelper::AddRoute(GetNode(), "/election" , m_face, 0);
  ndn::FibHelper::AddRoute(GetNode(), "/iAmLeader" , m_face, 0);
}

// Processing when application is stopped
void
BullyNonStarter::StopApplication()
{
  // cleanup ndn::App
  ndn::App::StopApplication();
  std::cout << "Stoping Application..\n";
}

void
BullyNonStarter::SendInterest()
{
  /////////////////////////////////////
  // Sending one Interest packet out //
  /////////////////////////////////////

  // Create and configure ndn::Interest

  auto interest = std::make_shared<ndn::Interest>("/prefix/sub");
  Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
  interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
  interest->setInterestLifetime(ndn::time::seconds(1));

  NS_LOG_DEBUG("Sending Interest packet for " << *interest);

  // Call trace (for logging purposes)
  m_transmittedInterests(interest, this, m_face);

  m_appLink->onReceiveInterest(*interest);
}

void
BullyNonStarter::OnInterest(std::shared_ptr<const ndn::Interest> interest)
{
  ndn::App::OnInterest(interest);
std::cout << "Interest " << *interest << " delivered to node " << GetNode() << std::endl;

  NS_LOG_DEBUG("Received Interest packet for " << interest->getName());

  // Note that Interests send out by the app will not be sent back to the app !

  auto data = std::make_shared<ndn::Data>();
  ndn::Name dataName(interest->getName());

const ndn::Name& name = interest->getName();
nfd::Name::const_iterator pointerToName = name.begin();
std::string tmp= "readIDs";
nfd::Name::Component Phase1(tmp);

//answering to prefix /readIDs

if ( *pointerToName == Phase1) {

  
  std::string trnsfToCmp;
  trnsfToCmp.append(std::to_string(GetNode()->GetId()));
  const ndn::Name::Component myID(trnsfToCmp);
  dataName.append(myID);

  data->setName(dataName);
  data->setFreshnessPeriod(ndn::time::milliseconds(1000));
  data->setContent(std::make_shared< ::ndn::Buffer>(1024));
  ndn::StackHelper::getKeyChain().sign(*data);
}

//answering to prefix /election

tmp="election";
nfd::Name::Component Phase2(tmp);
if ( *pointerToName == Phase2) {

  std::string trnsfToCmp="ok";
  const ndn::Name::Component myOK(trnsfToCmp);
  dataName.append(myOK);

  data->setName(dataName);
  data->setFreshnessPeriod(ndn::time::milliseconds(1000));
  data->setContent(std::make_shared< ::ndn::Buffer>(1024));
  ndn::StackHelper::getKeyChain().sign(*data);

  Simulator::ScheduleNow(&BullyNonStarter::IAmLeader, this);

}



  NS_LOG_DEBUG("Sending Data packet for " << data->getName());
//std::cout << "Data going out is " << *data << std::endl;
  // Call trace (for logging purposes)
  m_transmittedDatas(data, this, m_face);

  m_appLink->onReceiveData(*data);

}

// Callback that will be called when Data arrives
void
BullyNonStarter::OnData(std::shared_ptr<const ndn::Data> data)
{
  NS_LOG_DEBUG("Receiving Data packet for " << data->getName());

  std::cout << "DATA received for name " << data->getName() << std::endl;
}

void
BullyNonStarter::IAmLeader()
{
  auto interest = std::make_shared<ndn::Interest>("/iAmLeader");
  Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
  interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
  interest->setInterestLifetime(ndn::time::seconds(1));

  NS_LOG_DEBUG("Sending Interest packet for " << *interest);

  // Call trace (for logging purposes)
  m_transmittedInterests(interest, this, m_face);

  m_appLink->onReceiveInterest(*interest);
}

} // namespace ns3
