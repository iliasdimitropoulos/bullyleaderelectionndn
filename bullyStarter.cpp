// bullyStarter.cpp

#include "bully_starter.hpp"

#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"

#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"
#include "ns3/ndnSIM/helper/ndn-fib-helper.hpp"

#include "ns3/random-variable-stream.h"

NS_LOG_COMPONENT_DEFINE("BullyStarter");


namespace ns3 {

NS_OBJECT_ENSURE_REGISTERED(BullyStarter);
int numberOfNodes=4;
int array[2];
int candidateLeadersID=1;
int candidateLeadersPlace=0;
bool leaderFound=false;

ndn::Name::Component Phase1("readIDs");
ndn::Name::Component Phase2("election");
ndn::Name::Component Phase2OK("ok");

// register NS-3 type
TypeId
BullyStarter::GetTypeId()
{
  static TypeId tid = TypeId("BullyStarter").SetParent<ndn::App>().AddConstructor<BullyStarter>();
  return tid;
}

// Processing upon start of the application
void
BullyStarter::StartApplication()
{
  // initialize ndn::App
  ndn::App::StartApplication();
  std::cout << "Starting Application bullyStarter.." << Simulator::Now() << std::endl;

  // Add entry to FIB for `/prefix/sub`
  ndn::FibHelper::AddRoute(GetNode(), "/readIDs" , m_face, 0);
  ndn::FibHelper::AddRoute(GetNode(), "/election" , m_face, 0);
  ndn::FibHelper::AddRoute(GetNode(), "/election" , m_face, 0);

  //Simulator::Schedule(Seconds(1.0), &BullyStarter::SendInterest, this);
std::string readIDs;
for ( int i=0; i<numberOfNodes-1; i++) {
readIDs="/readIDs/";
readIDs.append(std::to_string(i));
Simulator::Schedule(Seconds(1.0), &BullyStarter::SendInterestWithPrefix, this, readIDs);
}

Simulator::Schedule(Seconds(2.0), &BullyStarter::FindLeader, this);

}

// Processing when application is stopped
void
BullyStarter::StopApplication()
{
  // cleanup ndn::App
  ndn::App::StopApplication();
  std::cout << "Stoping Application..\n";
}

void
BullyStarter::SendInterest()
{
  /////////////////////////////////////
  // Sending one Interest packet out //
  /////////////////////////////////////

  // Create and configure ndn::Interest

  auto interest = std::make_shared<ndn::Interest>("/readIDs");
  Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
  interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
  interest->setInterestLifetime(ndn::time::seconds(1));

  NS_LOG_DEBUG("Sending Interest packet for " << *interest);

  // Call trace (for logging purposes)
  m_transmittedInterests(interest, this, m_face);

  m_appLink->onReceiveInterest(*interest);
std::cout << "Interest " << *interest << " sent from node " << GetNode() << std::endl;
}

void
BullyStarter::OnInterest(std::shared_ptr<const ndn::Interest> interest)
{
  ndn::App::OnInterest(interest);

  NS_LOG_DEBUG("Received Interest packet for " << interest->getName());

  // Note that Interests send out by the app will not be sent back to the app !

  auto data = std::make_shared<ndn::Data>(interest->getName());
  data->setFreshnessPeriod(ndn::time::milliseconds(1000));
  data->setContent(std::make_shared< ::ndn::Buffer>(1024));
  ndn::StackHelper::getKeyChain().sign(*data);

  NS_LOG_DEBUG("Sending Data packet for " << data->getName());

  // Call trace (for logging purposes)
  m_transmittedDatas(data, this, m_face);

  m_appLink->onReceiveData(*data);
}

// Callback that will be called when Data arrives
void
BullyStarter::OnData(std::shared_ptr<const ndn::Data> data)
{
  NS_LOG_DEBUG("Receiving Data packet for " << data->getName());

  std::cout << "DATA received for name " << data->getName() << std::endl;

  ndn::Name dataName= data->getName();
  
  ndn::Name::const_iterator pointerToName = dataName.begin();

  if ( *pointerToName == Phase1) {
   array[dataName.at(1).toNumber()-48]=dataName.at(2).toNumber()-48;
  } else if ( *pointerToName == Phase2 && dataName.at(2) == Phase2OK) {
std::cout << "Supposed to print me... " << std::endl;
   leaderFound=true;
/*std::cout << "!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
for (int i=0; i<numberOfNodes-1; i++) {
std::cout << array[i] << std::endl;
}
std::cout << "!!!!!!!!!!!!!!!!!!!!!!" << std::endl;*/
  }

}

void
BullyStarter::SendInterestWithPrefix( std::string prefix)
{
  /////////////////////////////////////
  // Sending one Interest packet out //
  /////////////////////////////////////

  // Create and configure ndn::Interest

  auto interest = std::make_shared<ndn::Interest>(prefix);
  Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable>();
  interest->setNonce(rand->GetValue(0, std::numeric_limits<uint32_t>::max()));
  interest->setInterestLifetime(ndn::time::seconds(1));

  NS_LOG_DEBUG("Sending Interest packet for " << *interest);

  // Call trace (for logging purposes)
  m_transmittedInterests(interest, this, m_face);

  m_appLink->onReceiveInterest(*interest);
std::cout << "Interest " << *interest << " sent from node " << GetNode() << std::endl;
}

void
BullyStarter::FindLeader()
{
  //Check max id for leader election
if (!leaderFound) {
std::cout << "Printing the array with the IDs of other nodes" << std::endl;
for (int i=0; i<numberOfNodes-1; i++) {
std::cout << array[i] << std::endl;
}
  for ( int i=0; i<numberOfNodes-1; i++) {
   if (array[i]>candidateLeadersID) {
    candidateLeadersID=array[i];
    candidateLeadersPlace=i;
   }
  }
  std::cout << "Max number is " << candidateLeadersID << " at " << candidateLeadersPlace << std::endl;

  if (candidateLeadersID<GetNode()->GetId()) {
   std::string iAmLeader = "/iAmLeader";
   Simulator::ScheduleNow(&BullyStarter::SendInterestWithPrefix, this, iAmLeader);
  } else {
   std::string election="/election/";
   election.append(std::to_string(candidateLeadersPlace));
std::cout << "Sending out /election at " << Simulator::Now() << std::endl;
   Simulator::Schedule(Seconds(Simulator::Now().GetSeconds()+1.0), &BullyStarter::FindLeader, this);
   Simulator::ScheduleNow(&BullyStarter::SendInterestWithPrefix, this, election);

   array[candidateLeadersPlace]=0;
   candidateLeadersID=0;
  }

}
}

} // namespace ns3
