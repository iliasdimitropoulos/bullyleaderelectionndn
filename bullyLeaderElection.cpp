#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/point-to-point-module.h"

//for LinkStatusControl::FailLinks
#include "ns3/ndnSIM/helper/ndn-link-control-helper.hpp"

namespace ns3 {

void createRoutes(Ptr<Node> node_1, Ptr<Node> node_2, std::string prefix) {
  ndn::FibHelper fibHelper;
  ndn::FibHelper::AddRoute(node_1, prefix, node_2, 0);
  ndn::FibHelper::AddRoute(node_2, prefix, node_1, 0);
}

int
main(int argc, char* argv[])
{
  
  CommandLine cmd;
  cmd.Parse(argc, argv);

  Config::SetDefault("ns3::PointToPointNetDevice::DataRate", StringValue("1Mbps"));
  Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
  Config::SetDefault("ns3::QueueBase::MaxSize", StringValue("20p"));

  NodeContainer nodes;
  nodes.Create(4);

  PointToPointHelper p2p;
  p2p.Install(nodes.Get(0), nodes.Get(1));
  p2p.Install(nodes.Get(0), nodes.Get(2));
  p2p.Install(nodes.Get(0), nodes.Get(3));
  p2p.Install(nodes.Get(1), nodes.Get(2));
  p2p.Install(nodes.Get(1), nodes.Get(3));
  p2p.Install(nodes.Get(2), nodes.Get(3));

  ndn::StackHelper ndnHelper;
  ndnHelper.InstallAll();

  ndn::StrategyChoiceHelper::InstallAll("/readIDs", "localhost/nfd/strategy/myMulticast");
  ndn::StrategyChoiceHelper::InstallAll("/election", "localhost/nfd/strategy/myMulticast");
  ndn::StrategyChoiceHelper::InstallAll("/iAmLeader", "localhost/nfd/strategy/myMulticast");

  ndn::AppHelper bullyLeader("BullyStarter");
  bullyLeader.Install(nodes.Get(2));
  ndn::AppHelper bullyNonLeader("BullyNonStarter");
  bullyNonLeader.Install(nodes.Get(0));
  bullyNonLeader.Install(nodes.Get(1));
  bullyNonLeader.Install(nodes.Get(3));

  std::cout << "BullyNonStarterApp: " << nodes.Get(0) << "," << nodes.Get(1) << "," << nodes.Get(3) << "	BullyStarterApp: " << nodes.Get(2) << std::endl;

  ndn::GlobalRoutingHelper ndnGRH;
  ndnGRH.InstallAll();
  ndnGRH.AddOrigins("/readIDs/0", nodes.Get(0));
  ndnGRH.AddOrigins("/readIDs/1", nodes.Get(1));
  ndnGRH.AddOrigins("/readIDs/2", nodes.Get(3));
  ndnGRH.AddOrigins("/election/0", nodes.Get(0));
  ndnGRH.AddOrigins("/election/1", nodes.Get(1));
  ndnGRH.AddOrigins("/election/2", nodes.Get(3));
  ndnGRH.CalculateRoutes();

  //std::string prefix="/readIDs";
  //createRoutes(nodes.Get(0), nodes.Get(1), prefix);
  //createRoutes(nodes.Get(1), nodes.Get(2), prefix);
  //createRoutes(nodes.Get(0), nodes.Get(2), prefix);

  //Simulator::Schedule(Seconds(2.0), ndn::LinkControlHelper::FailLink, nodes.Get(0), nodes.Get(2));

  Simulator::Stop(Seconds(20.0));
  Simulator::Run();
  Simulator::Destroy();

  return 0;
}

} // namespace ns3

int
main(int argc, char* argv[])
{
  return ns3::main(argc, argv);
}
